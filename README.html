<!DOCTYPE html>
<!--
The MIT License (MIT)

Copyright (c) 2008-2019 Susam Pal
Copyright (c) 2022 Mohammed Anas (triallax)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->
<html>
<head>
<title>About kibou</title>
<meta charset="utf-8">
<link rel="icon" href="favicon.ico">
<link rel="stylesheet" href="css/base.css">
<link rel="stylesheet" href="css/info.css">
</head>
<body>

<!-- Navigation bar -->
<nav>
    <h1><a href="index.html">kibou</a></h1>
    <h2 id="unitTitle"></h2>
    <div id="navigation">
        <a href="index.html">Tutor</a>
        <a href="download.html">Download</a>
        <a href="LICENSE.html">License</a>
        <a href="README.html">About</a>
    </div>
</nav>

<!-- Main -->
<div id="mainWrapper">
<div id="main">
    <div id="content">

    <h2>About kibou</h2>

    <p>
    kibou is a touch typing tutor that runs on a web browser with JavaScript
    enabled. This software may be used to train your fingers to type without
    looking at the keys. After completing all the units available in this
    software, one would know the location of the keys on the keyboard through
    muscle memory, and would be able to effortlessly use the right finger for
    each key.
    </p>

    <h3>Contents</h3>
    <ul>
        <li><a href="#units">Units</a></li>
        <li><a href="#split">5&ndash;6 split vs. 6&ndash;7 split</a></li>
        <li><a href="#commands">Commands</a></li>
        <li><a href="#project-naming">Project naming</a></li>
        <li><a href="#license">License</a></li>
        <li><a href="#contact">Contact</a></li>
    </ul>

    <h3 id="units">Units</h3>
    <p>
    The typing lessons available in this software are divided into units
    and subunits. Each unit introduces a new set of keys to be
    practiced. For example, the first unit trains the fingers on how to
    press the keys in the home row. A couple of new keys are introduced
    with every new unit.
    </p><p>
    Each unit is divided into five subunits:
    </p>
    <ol>
        <li>Grip: Getting used to the keys</li>
        <li>Words: Learning to type words</li>
        <li>Control: Learning to stop abruptly in the middle of words</li>
        <li>Sentences: Learning to type sentences</li>
        <li>Test: Testing your proficiency level in the current unit</li>
    </ol>
    <p>
    One should move to the next lesson only after completing the current
    lesson satisfactorily. When a lesson is completed satisfactorily,
    the software suggests you to move to the next lesson.
    </p><p>
    While typing one should look at the computer monitor only. Fingers
    should always be placed on the home row as indicated in the first unit.
    If any key outside the home row has to be typed, one should return the
    finger to the home row immediately after striking that key.
    </p>

    <h3 id="split">5&ndash;6 split vs. 6&ndash;7 split</h3>
    <p>
    There are two popular ways to type the ten number keys on the top
    row.
    </p>
    <ol>
        <li>
        In a nontraditional style, '1' is typed with the little finger
        of the left hand, '2' is typed with the ring finger of the left
        hand, '5' is typed with the forefinger of the left hand and '6'
        is typed with the forefinger of the right hand. Thus, the number
        keys are split at 5&ndash;6 between the left hand and the right
        hand.
        </li><li>
        In the traditional style, '1' and '2' are typed with the little
        finger of the left hand, '6' is typed with the forefinger of the
        left hand and '7' is typed with the forefinger of the right
        hand.  In other words, the number keys are split at 6&ndash;7
        between the left hand and the right hand.
        </li>
    </ol>
    <p>
    In most keyboards, the '6' key is located closer to the 'F' key. As
    a result, the '6' key is more accessible to the left hand than the
    right hand. Therefore, many people prefer the traditional style with
    6&ndash;7 split. However, there are also many people who prefer the
    nontraditional style with 5&ndash;6 split. Which style you choose
    depends on your taste, convenience and the kind of keyboard you
    have. If you are confused about which style to choose, choose the
    traditional style with 6&ndash;7 split.
    </p><p>
    This software supports both styles for the number keys. In units
    16&ndash;20 which introduce the number keys, a link appears below
    the practice area to switch between the two styles.
    </p>

    <h3 id="commands">Commands</h3>
    <p>
    At any point while typing in the input box, you can type an input
    command.  The following is a list of the supported commands along
    with a description of what each command does.
    </p>
    <dl>
        <dt>restart</dt><dd>restart the current subunit</dd>
        <dt>rst</dt><dd>same as the 'restart' command</dd>
        <dt>fix</dt><dd>remove errors from the input text</dd>
        <dt>xxx</dt><dd>same as the 'fix' command</dd>
    </dl>
    <p>
    When you type a command in the input box, an error would be
    displayed because the software would be expecting you to type the
    text displayed above the input box instead of the command you are
    typing. This is okay and you can ignore the error and continue to
    type the command. As soon as you complete typing the command, your
    command would be executed.
    </p>

    <h3 id="project-naming">Project naming</h3>

    <p>
    kibou is the romanization of
    <a href="https://en.wiktionary.org/wiki/%E5%B8%8C%E6%9C%9B#Noun_2">the
    Japanese word 希望</a>, which means "hope." It also happens to kind of
    sound like the beginning of the word "keyboard," which seems somewhat
    fitting in this context I guess. :P
    </p>

    <h3 id="license">License</h3>
    <p>
    This is free and open source software. You can use, copy, modify,
    merge, publish, distribute, sublicense, and/or sell copies of it,
    under the terms of the <a href="LICENSE.html">MIT License</a>.
    </p>
    <p>
    This software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
    express or implied. See the <a href="LICENSE.html">MIT License</a>
    for details.
    </p>

    <h3 id="contact">Contact</h3>
    <p>
    To report any bugs, or suggest any improvements, please create a new
    issue at
    <a href="https://codeberg.org/triallax/kibou/issues">kibou's issue tracker</a>.
    </p>

    </div> <!-- End content -->
</div> <!-- End main -->
</div> <!-- End mainWrapper -->

<!-- Footer -->
<footer>
    <div id="navigation">
        <a href="index.html">Tutor</a>
        <a href="download.html">Download</a>
        <a href="https://codeberg.org/triallax/kibou">Source code</a>
        <a href="LICENSE.html">License</a>
        <a href="README.html">About</a>
    </div> <!-- End navigation -->
    <div id="copyright">
        <p>
        kibou 1.0.0<br>
        &copy; 2008-2019 <a href="https://susam.in">Susam Pal</a><br>
        &copy; 2022 Mohammed Anas (<a href="https://codeberg.org/triallax">triallax</a>)
        </p>
        <p>
        This is free and open source software. You can use, copy,
        modify, merge, publish, distribute, sublicense, and/or sell
        copies of it, under the terms of the
        <a href="LICENSE.html">MIT License</a>.
        </p>
    </div> <!-- End copyright -->
</footer> <!-- End footer -->
</body>
</html>
